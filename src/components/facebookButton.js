import React, { Component} from 'react';
import { FacebookProvider, Like } from 'react-facebook';

export default class FacebookButton extends Component {
  render() {
    return (
      <FacebookProvider language="fr_CA" appId="271410123423013">
        <Like 
          href="https://www.facebook.com/RevolvAir-2206425182723346/" 
          colorScheme="light" 
          showFaces="false" 
          layout="button_count" 
          size="large"
          share />
      </FacebookProvider>
    );
  }
}