import React, { Component } from 'react';
import logo from './assets/logo.svg';
import kibanaLogo from './assets/kibana-logo.png';
import elacticsearchLogo from './assets/elasticsearch-logo.png';
import './App.css';

import FacebookButton from './components/facebookButton';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">

        <div className="Facebook-button-container">
          <FacebookButton />
        </div>

        <div className="Header-content" >
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">
            RevolvAir
          </h1>
          <p className="Subtitle">
            Analyse et protection de la qualité de l'air
          </p>
        </div>

        </header>
        <div className="App-content">

          <h1 className="Content-title">
            Droit à un environnement sain
          </h1>

          <div className="Panel">
            <table>
              <tbody>
                <tr>
                  <td>
                    Qualité de l'air
                  </td>
                </tr>
                <tr>
                  <td>
                    Données ouvertes
                  </td>
                </tr>
                <tr>
                  <td>
                    Objets connectés
                  </td>
                </tr>
                <tr>
                  <td>
                    Projet scientifique
                  </td>
                </tr>
                <tr>
                  <td>
                    Projet technologique
                  </td>
                </tr>
                <tr>
                  <td>
                    Projet communautaire
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <h2 className="Content-title">
            Propulsé par
          </h2>

          <div className="Content-technos">
            <img src={kibanaLogo} className="App-logo" alt="kibana" />
            <img src={elacticsearchLogo} className="App-logo" alt="logo" />
          </div>
        </div>



      </div>
    );
  }
}

export default App;
